package com.nokia.business;

import com.nokia.dao.ManufacturerDAOImpl;
import com.nokia.dao.PartDAOImpl;
import com.nokia.dao.PartManufacturerDAOImpl;
import com.nokia.entity.Company;
import com.nokia.entity.Manufacturer;
import com.nokia.entity.Part;
import com.nokia.entity.PartManufacturer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class PartManufacturerBusinessTest {
    PartDAOImpl partDAO;
    ManufacturerDAOImpl manufacturerDAO;
    PartManufacturerDAOImpl partManufacturerDAO;

    PartManufacturerBusinessImpl partManufacturerBusiness;

    Part part;

    Manufacturer manufacturer;
    Company company;
    List<Manufacturer> manufacturerList;
    List<PartManufacturer> partManufacturerList;

    @Before
    public void setup() {
        partDAO = Mockito.mock(PartDAOImpl.class);
        manufacturerDAO = Mockito.mock(ManufacturerDAOImpl.class);
        partManufacturerDAO = Mockito.mock(PartManufacturerDAOImpl.class);
        partManufacturerBusiness = new PartManufacturerBusinessImpl(partDAO, manufacturerDAO, partManufacturerDAO);

        part = new Part(1L, "Part 1", null);

        company = new Company(1L, 140.0, "Nokia", null);
        manufacturer = new Manufacturer(1L, "Manufacturer 1", null);

        manufacturerList = Arrays.asList(
                new Manufacturer(1L, "Manufacturer 1", null),
                new Manufacturer(2L, "Manufacturer 2", null),
                new Manufacturer(3L, "Manufacturer 3", null),
                new Manufacturer(4L, "Manufacturer 4", null)
        );

        partManufacturerList = Arrays.asList(
                new PartManufacturer(1L, part, manufacturerList.get(0), 10, 15f),
                new PartManufacturer(2L, part, manufacturerList.get(1), 6, 7.5f),
                new PartManufacturer(3L, part, manufacturerList.get(2), 5, 6.7f),
                new PartManufacturer(4L, part, manufacturerList.get(3), 12, 8.5f)
        );
    }

    @Test
    public void testProcessPartManufacturerQuantity_When_FetchedPartManufacturer_Exists_Return_PartManufacturer() {
        String partName = "Part 1";
        String manufacturerName = "Manufacturer 1";
        int quantity = 10;
        float price = 5.5f;

        Optional<Part> part = Optional.of(new Part(1L, partName, null));
        Optional<Manufacturer> manufacturer = Optional.of(new Manufacturer(1L, manufacturerName, null));

        PartManufacturer inputPartManufacturer = new PartManufacturer(null, part.get(), manufacturer.get(), 20, 10.7f);
        PartManufacturer fetchedPartManufacturer = new PartManufacturer(1L, part.get(), manufacturer.get(), quantity, price);

        PartManufacturer expectedPartManufacturer = new PartManufacturer(1L, part.get(), manufacturer.get(), 30, 10.7f);
        PartManufacturer actualPartManufacturer = partManufacturerBusiness.processPartManufacturerQuantity(Optional.of(fetchedPartManufacturer), inputPartManufacturer);

        Assert.assertEquals(expectedPartManufacturer.getPart(), actualPartManufacturer.getPart());
        Assert.assertEquals(expectedPartManufacturer.getManufacturer(), actualPartManufacturer.getManufacturer());
        Assert.assertEquals(expectedPartManufacturer.getQuantity(), actualPartManufacturer.getQuantity());
        Assert.assertEquals(expectedPartManufacturer.getPrice(), actualPartManufacturer.getPrice(), 0.0);
    }

    @Test
    public void testProcessPartManufacturerQuantity_When_FetchedPartManufacturer_Does_Not_Exists_Return_PartManufacturer() {
        String partName = "Part 1";
        String manufacturerName = "Manufacturer 1";
        int quantity = 10;
        float price = 5.5f;

        Optional<Part> part = Optional.of(new Part(1L, partName, null));
        Optional<Manufacturer> manufacturer = Optional.of(new Manufacturer(1L, manufacturerName, null));

        PartManufacturer inputPartManufacturer = new PartManufacturer(null, part.get(), manufacturer.get(), quantity, price);
        Optional<PartManufacturer> fetchedPartManufacturer = Optional.empty();

        PartManufacturer actualPartManufacturer = partManufacturerBusiness.processPartManufacturerQuantity(fetchedPartManufacturer, inputPartManufacturer);

        Assert.assertEquals(inputPartManufacturer.getPart(), actualPartManufacturer.getPart());
        Assert.assertEquals(inputPartManufacturer.getManufacturer(), actualPartManufacturer.getManufacturer());
        Assert.assertEquals(inputPartManufacturer.getQuantity(), actualPartManufacturer.getQuantity());
        Assert.assertEquals(inputPartManufacturer.getPrice(), actualPartManufacturer.getPrice(), 0.0);
    }


    @Test
    public void testGetPartManufacturerWithUpdatedQuantiity_When_PartManufacerList_Contains_BoughtPartManufacturer_Then_Return_PartManufacturerWithUpdatedQuantiity() {
        PartManufacturer boughtPartManufacturer =
                new PartManufacturer(1L, part, manufacturer, 7, 15f);

        PartManufacturer expectedPartManufacturerWithQuantity =
                new PartManufacturer(1L, part, manufacturer, 3, 15f);

        PartManufacturer actualPartManufacturerWithQuantity =
                partManufacturerBusiness.getPartManufacturerWithUpdatedQuantiity(partManufacturerList, boughtPartManufacturer);

        Assert.assertNotNull(actualPartManufacturerWithQuantity);
        Assert.assertEquals(expectedPartManufacturerWithQuantity.getId(), actualPartManufacturerWithQuantity.getId());
        Assert.assertEquals(expectedPartManufacturerWithQuantity.getPart(), actualPartManufacturerWithQuantity.getPart());
        Assert.assertEquals(expectedPartManufacturerWithQuantity.getManufacturer(), actualPartManufacturerWithQuantity.getManufacturer());
        Assert.assertEquals(expectedPartManufacturerWithQuantity.getQuantity(), actualPartManufacturerWithQuantity.getQuantity());
        Assert.assertEquals(expectedPartManufacturerWithQuantity.getPrice(), actualPartManufacturerWithQuantity.getPrice(), 0.0);
    }

    @Test
    public void testGetPartManufacturerWithUpdatedQuantiity_When_PartManufacerList_Does_Not_Contains_BoughtPartManufacturer_Then_Return_Null() {
        PartManufacturer boughtPartManufacturer =
                new PartManufacturer(10L, part, manufacturer, 7, 15f);

        PartManufacturer actualPartManufacturerWithQuantity =
                partManufacturerBusiness.getPartManufacturerWithUpdatedQuantiity(partManufacturerList, boughtPartManufacturer);

        Assert.assertNull(actualPartManufacturerWithQuantity);
    }


    @Test
    public void testGetPartManufacturer_When_PartManufacturer_Exists_Return_PartManufacturer() {
        String partName = "Part 1";
        String manufacturerName = "Manufacturer 1";

        Mockito.when(partManufacturerDAO.getByPartAndManufacturer(partName, manufacturerName))
                .thenReturn(Optional.of(partManufacturerList.get(0)));

        Optional<PartManufacturer> expectedPartManufacturer = Optional.of(partManufacturerList.get(0));
        Optional<PartManufacturer> actualPartManufacturer = partManufacturerBusiness.getPartManufacturer(partName, manufacturerName);

        Assert.assertEquals(expectedPartManufacturer.get().getId(), actualPartManufacturer.get().getId());
        Assert.assertEquals(expectedPartManufacturer.get().getPart(), actualPartManufacturer.get().getPart());
        Assert.assertEquals(expectedPartManufacturer.get().getManufacturer(), actualPartManufacturer.get().getManufacturer());
        Assert.assertEquals(expectedPartManufacturer.get().getQuantity(), actualPartManufacturer.get().getQuantity());
        Assert.assertEquals(expectedPartManufacturer.get().getPrice(), actualPartManufacturer.get().getPrice(), 0.0);
    }

    @Test
    public void testGetPartManufacturer_When_RunTimeException_Thrown_Return_Empty_PartManufacturer() {
        String partName = "Part 1";
        String manufacturerName = "Manufacturer 1";

        Mockito.when(partManufacturerDAO.getByPartAndManufacturer(partName, manufacturerName))
                .thenThrow(RuntimeException.class);

        Optional<PartManufacturer> actualPartManufacturer = partManufacturerBusiness.getPartManufacturer(partName, manufacturerName);

        Assert.assertEquals(Optional.empty(), actualPartManufacturer);
    }
}
